package com.muhardin.endy.belajar.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class BeforeAdviceTransaction implements MethodInterceptor {

    @Override
    public Object invoke(MethodInvocation method) throws Throwable {
        System.out.println("Nama method asli : "+method.getMethod().getName());
        System.out.println("Jumlah argumen : "+method.getArguments().length);

        int numArg = 1;
        for(Object arg : method.getArguments()){
            System.out.println("Argumen "+numArg+" = "+arg);
            numArg++;
        }

        System.out.println("==== Begin Transaction ====");
        Object returnValue = method.proceed(); // menjalankan method aslinya
        System.out.println("==== Commit / Rollback Transaction ====");

        System.out.println("Return value (kalau ada) "+returnValue);
        return returnValue;
    }
    
}
