package com.muhardin.endy.belajar.aop;

import java.math.BigDecimal;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoAopSpring {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("aop.xml");
        FormSetorTunai formSetorTunai = ac.getBean(FormSetorTunai.class);
        formSetorTunai.inputSetoran("Customer 001", new BigDecimal(500000));
    }
}
