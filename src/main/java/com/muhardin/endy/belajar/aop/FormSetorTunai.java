package com.muhardin.endy.belajar.aop;

import java.math.BigDecimal;

import lombok.Setter;

@Setter
public class FormSetorTunai {
    private BankService bankService;

    public void inputSetoran(String customer, BigDecimal nilai){
        System.out.println("Memproses setoran "+customer+" sejumlah "+nilai);
        bankService.setor(nilai);
    }
}
