package com.muhardin.endy.belajar.aop;

import java.math.BigDecimal;

public class DemoNoAop {
    public static void main(String[] args) {
        BankService bankService = new BankService();
        FormSetorTunai formSetorTunai = new FormSetorTunai();

        formSetorTunai.setBankService(bankService);
        formSetorTunai.inputSetoran("Endy", new BigDecimal(100000));
    }
}
