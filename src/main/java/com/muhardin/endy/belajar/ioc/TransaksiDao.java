package com.muhardin.endy.belajar.ioc;

public class TransaksiDao {

    private KoneksiDatabase koneksi;

    public TransaksiDao(){
        koneksi = new KoneksiDatabase();
    }

    public TransaksiDao(KoneksiDatabase k){
        koneksi = k;
    }

    public void save(Transaksi transaksi){
        System.out.println("Connect ke database");
        System.out.println("insert into transaksi (waktu, id_customer, nilai) values (?,?)");
        System.out.println("Disconnect");
    }
}
