package com.muhardin.endy.belajar.ioc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerDao {

    @Autowired
    private KoneksiDatabase koneksi;

    // tanpa injection
    public CustomerDao(){
        koneksi = new KoneksiDatabase();
    }

    // constructor injection
    public CustomerDao(KoneksiDatabase k){
        koneksi = k;
    }

    // setter injection
    public void setKoneksiDatabase(KoneksiDatabase k) {
        koneksi = k;
    }

    public void save(Customer c){
        System.out.println("Connect ke database");
        koneksi.connect();
        System.out.println("Menyimpan data customer "+c.getNama());
        String sql = "insert into customer (nama, email) values (?,?)";
        koneksi.jalankan(sql);
        System.out.println("Disconnect");
        koneksi.disconnect();
    }
}
