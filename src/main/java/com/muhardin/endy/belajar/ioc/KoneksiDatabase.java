package com.muhardin.endy.belajar.ioc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter @Setter
public class KoneksiDatabase {

    @Value("${database.host}")
    private String host;
    @Value("${database.port}")
    private String port;
    @Value("${database.username}")
    private String username;
    @Value("${database.password}")
    private String password;

    public void connect(){
        System.out.println("Connect ke database "+host+":"+port);
    }

    public void jalankan(String sql){
        System.out.println("Menjalankan sql : "+ sql);
    }

    public void disconnect(){

    }
}
