package com.muhardin.endy.belajar.ioc;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Customer {
    private String nama;
    private String email;
}
