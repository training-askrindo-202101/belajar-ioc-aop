package com.muhardin.endy.belajar.ioc;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Transaksi {
    private LocalDateTime waktuTransaksi;
    private Customer customer;
    private BigDecimal nilaiTransaksi;
}
