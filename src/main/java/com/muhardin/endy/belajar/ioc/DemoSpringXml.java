package com.muhardin.endy.belajar.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoSpringXml {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("ioc.xml");

        CustomerDao customerDao = (CustomerDao) ac.getBean("customerDao");

        Customer c = new Customer();
        c.setNama("Endy");
        c.setEmail("endy@muhardin.com");
        customerDao.save(c);
    }
}
