package com.muhardin.endy.belajar.ioc;

public class Demo {
    public static void main(String[] args) {
        // versi lama
        KoneksiDatabase k = new KoneksiDatabase();
        k.setHost("localhost");
        k.setPort("3306");

        // versi baru sudah bisa pooling
        //KoneksiDatabasePooling k = new KoneksiDatabasePooling();

        // versi koneksi yang sudah pakai transaction (begin/commit/rollback)
        //KoneksiDenganTransaction k = new KoneksiDenganTransaction();

        CustomerDao customerDao = new CustomerDao(k); // share object
        CustomerDao customerDao2 = new CustomerDao(new KoneksiDatabase()); // koneksi per request

        TransaksiDao transaksiDao = new TransaksiDao(k); // share object koneksi

        Customer c = new Customer();
        c.setNama("Endy");
        c.setEmail("endy@muhardin.com");
        customerDao.save(c);
    }
}
