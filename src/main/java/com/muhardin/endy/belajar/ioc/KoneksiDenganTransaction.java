package com.muhardin.endy.belajar.ioc;

public class KoneksiDenganTransaction extends KoneksiDatabase {
    public void jalankan(String sql){
        System.out.println("Begin transaction");
        System.out.println("Menjalankan sql : "+ sql);
        System.out.println("Commit transaction");
    }
}
